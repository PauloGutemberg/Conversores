//
//  ViewController.swift
//  Conversores
//
//  Created by Paulo Gutemberg on 24/04/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var lbUnidade: UILabel!
	@IBOutlet weak var tfValor: UITextField!
	@IBOutlet weak var btnUnidade1: UIButton!
	@IBOutlet weak var btnUnidade2: UIButton!
	
	
	@IBOutlet weak var lbResultado: UILabel!
	@IBOutlet weak var lbUnidadeResultado: UILabel!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
	}

	@IBAction func mostrarProxima(_ sender: UIButton) {
		switch lbUnidade.text! {
		case "Temperatura":
			lbUnidade.text = "Peso"
			btnUnidade1.setTitle("Kilograma", for: .normal)
			btnUnidade2.setTitle("Libra", for: .normal)
		case "Peso":
			lbUnidade.text = "Moeda"
			btnUnidade1.setTitle("Real", for: .normal)
			btnUnidade2.setTitle("Dolar", for: .normal)
		case "Moeda":
			lbUnidade.text = "Distancia"
			btnUnidade1.setTitle("Metro", for: .normal)
			btnUnidade2.setTitle("Kilometro", for: .normal)
		default :
			lbUnidade.text = "Temperatura"
			btnUnidade1.setTitle("Celsius", for: .normal)
			btnUnidade2.setTitle("Farenheint", for: .normal)
		}
		
		converter(nil)
		
	}
	
	@IBAction func converter(_ sender: UIButton?) {
		//if let so funciona dentro desse scopo, dentro desse bloco
		if let sender = sender {
			if sender == btnUnidade1 {
				btnUnidade2.alpha = 0.5
			}else{
				btnUnidade1.alpha = 0.5
			}
			sender.alpha = 1.0
			
			switch lbUnidade.text! {
			case "Temperatura":
				calcTemperatura()
			case "Peso":
				calcPeso()
			case "Moeda":
				calcMoeda()
			default :
				calcDistancia()
			}
		}
		//teclado desaparece
		view.endEditing(true)
		
		let resultado = Double(lbResultado.text!)!
		lbResultado.text = String(format: "%.2f", resultado)
	}
	
	func calcTemperatura(){
		/*Durante a conversao do textField de valor ele retorna um optional
		entao ele tenta desembrulhar o textfield de valor e atraibuir a uma variavel
		a diferenca e que o guard let mantem a variavel pro resto do codigo
		
		caso de errado o desembrulho da funcao ele simplismente sai da calcTemperatura() com o return
		*/
		guard let temperatura = Double(tfValor.text!) else {return}
		if(btnUnidade1.alpha == 1.0){
			lbUnidadeResultado.text = "Farenheint"
			lbResultado.text = String(temperatura * 1.8 + 32.0)
		}else{
			lbUnidadeResultado.text = "Celsius"
			lbResultado.text = String((temperatura - 32.0 ) / 1.8)
		}
	}
	
	func calcPeso(){
		guard let peso = Double(tfValor.text!) else {return}
		if(btnUnidade1.alpha == 1.0){
			lbUnidadeResultado.text = "Libra"
			lbResultado.text = String(peso / 2.2046)
		}else{
			lbUnidadeResultado.text = "Kilograma"
			lbResultado.text = String(peso * 2.2046)
		}
	}
	
	func calcMoeda(){
		guard let moeda = Double(tfValor.text!) else {return}
		if(btnUnidade1.alpha == 1.0){
			lbUnidadeResultado.text = "Dolar"
			lbResultado.text = String(moeda / 3.5)
		}else{
			lbUnidadeResultado.text = "Real"
			lbResultado.text = String(moeda * 3.5)
		}
	}
	
	func calcDistancia(){
		guard let distancia = Double(tfValor.text!) else {return}
		if(btnUnidade1.alpha == 1.0){
			lbUnidadeResultado.text = "Kilometro"
			lbResultado.text = String(distancia / 1000.0)
		}else{
			lbUnidadeResultado.text = "Metros"
			lbResultado.text = String(distancia * 1000.0)
		}
	}
	
}

